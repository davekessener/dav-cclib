#ifndef DAVE_UTIL_STRINGIFY_H
#define DAVE_UTIL_STRINGIFY_H

#include <string>
#include <sstream>

#include <dave/types.h>
#include <dave/fix.h>

namespace dave
{
	namespace util
	{
		namespace impl
		{
			template<typename S>
			void stringify(S&)
			{
			}

			template<typename S, typename T, typename ... TT>
			void stringify(S& ss, const T& v, const TT& ... a)
			{
				ss << v;

				stringify(ss, a...);
			}
		}

		template<typename ... T>
		std::string stringify(const T& ... a)
		{
			std::stringstream ss;

			impl::stringify(ss, a...);

			return ss.str();
		}
	}
}

#endif

