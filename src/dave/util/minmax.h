#ifndef DAVE_UTIL_MINMAX_H
#define DAVE_UTIL_MINMAX_H

namespace dave
{
	namespace util
	{
		namespace detail
		{
			template<typename ...>
			struct NumericTypeUnion;

			template<typename T1, typename T2, typename ... TT>
			struct NumericTypeUnion<T1, T2, TT...>
			{
				typedef typename NumericTypeUnion<T2, TT...>::Type t_t;

				typedef decltype(mpl::declval<T1>() + mpl::declval<t_t>()) Type;
			};

			template<typename T>
			struct NumericTypeUnion<T>
			{
				typedef T Type;
			};

			template<typename R, typename T>
			R min_impl(const T& v)
			{
				return v;
			}

			template<typename R, typename T1, typename T2, typename ... TT>
			R min_impl(const T1& t1, const T2& t2, const TT& ... a)
			{
				return min_impl<R>((t1 < t2 ? t1 : t2), a...);
			}

			template<typename R, typename T>
			R max_impl(const T& v)
			{
				return v;
			}

			template<typename R, typename T1, typename T2, typename ... TT>
			R max_impl(const T1& t1, const T2& t2, const TT& ... a)
			{
				return max_impl<R>((t1 > t2 ? t1 : t2), a...);
			}
		}

		template<typename ... T>
		typename detail::NumericTypeUnion<T...>::Type min(const T& ... a)
		{
			typedef typename detail::NumericTypeUnion<T...>::Type return_t;

			return detail::min_impl<return_t>(a...);
		}

		template<typename ... T>
		typename detail::NumericTypeUnion<T...>::Type max(const T& ... a)
		{
			typedef typename detail::NumericTypeUnion<T...>::Type return_t;

			return detail::max_impl<return_t>(a...);
		}
	}
}

#endif

