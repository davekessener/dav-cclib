#ifndef DAVE_TYPES_H
#define DAVE_TYPES_H

#include <cstdint>

namespace dave
{
	typedef std::uint8_t  uint8_t;
	typedef std::uint16_t uint16_t;
	typedef std::uint32_t uint32_t;
	typedef std::uint64_t uint64_t;

	typedef std::int8_t  int8_t;
	typedef std::int16_t int16_t;
	typedef std::int32_t int32_t;
	typedef std::int64_t int64_t;

	typedef unsigned uint;
	
	typedef uint8_t u8;
	typedef uint16_t u16;
	typedef uint32_t u32;
	typedef uint64_t u64;
	
	typedef decltype(nullptr) nullptr_t;
}

#endif

