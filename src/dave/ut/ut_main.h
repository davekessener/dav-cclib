#ifndef DAVE_UT_UTMAIN_H
#define DAVE_UT_UTMAIN_H

#define UT_IMPL_IS_MAIN

#include <dave/ut/ut_types.h>
#include <dave/ut/ut_base.h>

#define RUN_ALL_UT_AT(s) ::dave::ut::_the_manager.run(s)
#define RUN_ALL_UT RUN_ALL_UT_AT(::std::cout)

namespace dave { namespace ut {
	SuiteManager<TestSuite<void>> _the_manager;
}}

#endif

