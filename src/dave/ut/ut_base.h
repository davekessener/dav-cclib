#ifndef DAVE_UT_UTBASE_H
#define DAVE_UT_UTBASE_H

#include <dave/util/stringify.h>

#include <dave/ut/ut_types.h>

#include <dave/mpl/bool.h>

#define _UT_IMPL_DECLARE_UT_SUITE(cname,desc) \
class cname : public ::dave::ut::TestSuite<void> { \
	public: \
		cname( ); \
		const ::dave::ut::string_t& description( ) const override { \
			static ::dave::ut::string_t d{desc}; \
			return d; \
		} \
}

#ifdef UT_IMPL_IS_MAIN
#	define DECLARE_UT_SUITE(cname,desc) _UT_IMPL_DECLARE_UT_SUITE(cname,desc) _ut_instance_##cname
#else
#	define DECLARE_UT_SUITE(cname,desc) _UT_IMPL_DECLARE_UT_SUITE(cname,desc)
#endif

#define DEFINE_UT_SUITE(cname) \
	cname :: cname ( ) : ::dave::ut::TestSuite<void>( #cname )

#define UT_CASE(name) \
	if(!mData && !::dave::mpl::Equals<_ut_data_t, ::dave::ut::TestSuite<void>::_ut_data_t>::Value) mData = new _ut_data_t; \
	this->mTests[name] = [](_ut_data_t *data)

#define UT_DATA struct _ut_data_t

#define UT_SETUP mSetup = [](_ut_data_t *data)
#define UT_TEARDOWN mTeardown = [](_ut_data_t *data)
#define UT_GLOBAL_SETUP mGblSetup = [](_ut_data_t *data)
#define UT_GLOBAL_TEARDOWN mGblTeardown = [](_ut_data_t *data)

#define UT_FAIL(...) throw ::dave::ut::UTException(__FILE__, __LINE__, __VA_ARGS__)

#define UT_ASSERT(c,...) do { \
	if(!(c)) { \
		::dave::ut::string_t s{::dave::util::stringify(__VA_ARGS__)}; \
		if(s.empty()) {\
			s = #c ; \
		} else { \
			s += " (" #c ")"; \
		} \
		UT_FAIL(s); \
	} \
} while(0)

#define UT_ASSERT_EQ(a,b) do { \
	auto va{(a)}; \
	auto vb{(b)}; \
	if(!(va == vb)) { \
		UT_FAIL(va, " == ", vb, " (" #a " == " #b ")"); \
	} \
} while(0)

namespace dave
{
	namespace ut
	{
		namespace impl
		{
			template<typename F>
			struct GetArgType
			{
				template<typename C, typename A>
				static A f(void (C::*)(A *));

				template<typename C, typename A>
				static A f(void (C::*)(A *) const);

				typedef decltype(f(&F::operator())) Type;
			};
		}

		template<typename T>
		class SuiteManager
		{
			public:
				void reg(T *ts)
				{
					mSuites.push_back(ts);
				}

				template<typename S>
				void run(S& os)
				{
					vector_t<string_t> errors;
					int nerr = 0;
					uint total = 0;

					os << "Running ut: ";

					for(auto& p : mSuites)
					{
						vector_t<string_t> s;

						p->run(os, s);

						total += p->size();

						if(!s.empty())
						{
							errors.push_back(util::stringify(p->name(), " - ", p->description()));

							for(const auto& e : s)
							{
								errors.push_back(util::stringify("    ", e));

								++nerr;
							}
						}
					}

					os << "\n";

					if(!errors.empty())
					{
						for(const auto& e : errors)
						{
							os << e << "\n";
						}
					}

					os << "Finished " << mSuites.size() << " suites totalling " << total << " tests with " << nerr << " errors.\n";
				}

			private:
				vector_t<T *> mSuites;
		};

		class UTException : public exception_t
		{
			public:
				template<typename ... T>
				UTException(const char *f, int l, const T& ... a)
					: mMsg(util::stringify(a..., " [", f, ":", l, "]"))
				{ }

				const char *what( ) const noexcept override { return mMsg.data(); }

			private:
				string_t mMsg;
		};

		template<typename>
		class TestSuite
		{
			protected:
			struct _ut_data_t { };

			private:
			typedef function_t<void(void *)> _ut_cb_f;

			struct _ut_cb
			{
				_ut_cb_f cb;

				template<typename F>
				void operator=(F f)
				{
					typedef typename impl::GetArgType<F>::Type arg_t;

					cb = [f](void *data) { f(reinterpret_cast<arg_t *>(data)); };
				}

				void operator()(void *ptr)
				{
					if(static_cast<bool>(cb))
					{
						cb(ptr);
					}
				}
			};

			struct _ut_test_case
			{
				string_t name;
				_ut_cb cb;

				_ut_test_case(const string_t& name) : name(name) { }
			};

			struct _ut_test_list
			{
				vector_t<_ut_test_case *> tests;

				_ut_cb& operator[](const string_t& name)
				{
					auto *t = new _ut_test_case(name);

					tests.push_back(t);

					return t->cb;
				}
			};

			public:
				TestSuite(const string_t&);

				template<typename S>
					void run(S&, vector_t<string_t>&);

				const string_t& name( ) const { return mName; }
				uint size( ) const { return mTests.tests.size(); }

				virtual const string_t& description( ) const = 0;

			protected:
				string_t mName;
				_ut_test_list mTests;
				void *mData;
				_ut_cb mSetup, mTeardown, mGblSetup, mGblTeardown;
		};

		extern SuiteManager<TestSuite<void>> _the_manager;

		template<typename T>
		TestSuite<T>::TestSuite(const string_t& name)
			: mName(name)
		{
			mData = nullptr;

			_the_manager.reg(this);
		}

		template<typename T>
		template<typename S>
		void TestSuite<T>::run(S& os, vector_t<string_t>& err)
		{
			mGblSetup(mData);

			for(auto& c : mTests.tests)
			{
				string_t error = "";

				mSetup(mData);

				try
				{
					c->cb(mData);
				}
				catch(const UTException& e)
				{
					error = e.what();
				}
				catch(const exception_t& e)
				{
					error = util::stringify("EXCEPTION (", e.what(), ")");
				}
				catch(...)
				{
					error = "FATAL!";
				}

				mTeardown(mData);

				if(error.empty())
				{
					os << ".";
				}
				else
				{
					os << "F";

					err.push_back(util::stringify(c->name, ": ", error));
				}
			}

			mGblTeardown(mData);
		}
	}
}

#endif

