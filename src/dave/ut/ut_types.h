#ifndef DAVE_UT_UTTYPES_H
#define DAVE_UT_UTTYPES_H

#include <string>
#include <vector>
#include <functional>
#include <exception>

namespace dave
{
	namespace ut
	{
		template<typename T>
		using vector_t = std::vector<T>;

		typedef std::string string_t;
		
		template<typename F>
		using function_t = std::function<F>;

		typedef std::exception exception_t;
	}
}

#endif

