#ifndef DAVE_FIX_H
#define DAVE_FIX_H

#include <ios>

namespace std
{
	template<typename T, typename C>
	basic_ostream<T, C>& operator<<(basic_ostream<T, C>& os, nullptr_t)
	{
		return os << (void *) nullptr;
	}
}

#endif

