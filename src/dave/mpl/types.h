#ifndef DAVE_MPL_TYPES_H
#define DAVE_MPL_TYPES_H

#include <dave/types.h>

namespace dave
{
	namespace mpl
	{
		struct Nil { };

		template<bool V>
		struct Bool
		{
			typedef Bool<V> Type;
			static constexpr bool Value = V;
		};

		typedef Bool<true> True;
		typedef Bool<false> False;

		template<int V>
		struct Int
		{
			typedef Int<V> Type;
			static constexpr int Value = V;
		};

		template<typename T>
		struct Identity
		{
			typedef T Type;
		};

		template<typename T>
		using identity = typename Identity<T>::Type;

		template<typename T>
		T&& declval( )
		{
			return *((T *) nullptr);
		}

		template<typename T>
		T&& forward(identity<T>&& v)
		{
			return static_cast<identity<T>&&>(v);
		}
	}
}

#endif

