#ifndef DAVE_MPL_ISITERABLE_H
#define DAVE_MPL_ISITERABLE_H

#include <dave/mpl/types.h>
#include <dave/mpl/decay.h>
#include <dave/mpl/bool.h>

namespace dave
{
	namespace mpl
	{
		template<typename T>
		struct IsIterable
		{
			static False f(...);

			template<typename TT>
			static auto f(TT *p) -> Equals<decltype(*p->begin()), decltype(*p->end())>;

			typedef decltype(f(declval<T *>())) Type;
			static constexpr bool Value = Type::Value;
		};
	}
}

#endif

