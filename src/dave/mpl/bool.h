#ifndef DAVE_MPL_BOOL_H
#define DAVE_MPL_BOOL_H

#include <dave/mpl/types.h>

namespace dave
{
	namespace mpl
	{
		namespace impl
		{
			template<bool, typename T1, typename T2>
			struct If : T2
			{
				typedef typename T2::Type Type;
			};

			template<typename T1, typename T2>
			struct If<true, T1, T2>
			{
				typedef typename T1::Type Type;
			};
		}

		template<typename C, typename T1, typename T2>
		struct If : impl::If<C::Value, T1, T2>
		{
		};

		template<typename, typename>
		struct Equals : False
		{
		};

		template<typename T>
		struct Equals<T, T> : True
		{
		};

		template<typename ...>
		struct AllEquals;

		template<typename T>
		struct AllEquals<T> : True
		{
		};

		template<typename T1, typename T2, typename ... TT>
		struct AllEquals<T1, T2, TT...> : False
		{
		};

		template<typename T, typename ... TT>
		struct AllEquals<T, T, TT...> : AllEquals<T, TT...>
		{
		};

		template<typename T>
		struct Not : Bool<!(T::Value)>
		{
		};

		namespace impl
		{
			template<bool V, typename ... T>
			struct And : False
			{
			};

			template<typename T, typename ... TT>
			struct And<true, T, TT...> : And<T::Value, TT...>
			{
			};

			template<>
			struct And<true> : True
			{
			};

			template<bool V, typename ... T>
			struct Or : True
			{
			};

			template<typename T, typename ... TT>
			struct Or<false, T, TT...> : Or<T::Value, TT...>
			{
			};

			template<>
			struct Or<false> : False
			{
			};
		}

		template<typename ...>
		struct And;

		template<typename T, typename ... TT>
		struct And<T, TT...> : impl::And<T::Value, TT...>
		{
		};

		template<typename ...>
		struct Or;

		template<typename T, typename ... TT>
		struct Or<T, TT...> : impl::Or<T::Value, TT...>
		{
		};
	}
}

#endif

