#ifndef DAVE_MPL_ENABLEIF_H
#define DAVE_MPL_ENABLEIF_H

#include <dave/mpl/types.h>

namespace dave
{
	namespace mpl
	{
		namespace impl
		{
			template<bool>
			struct EnableIf
			{
			};

			template<>
			struct EnableIf<true>
			{
				typedef True Type;
			};
		}

		template<typename T>
		struct EnableIf : impl::EnableIf<T::Value>
		{
		};

		template<typename T>
		using enable_if = typename EnableIf<T>::Type;
	}
}

#endif

