#ifndef DAVE_MPL_TRANSFORMINGITERATOR_H
#define DAVE_MPL_TRANSFORMINGITERATOR_H

#include <dave/mpl/types.h>
#include <dave/mpl/function_traits.h>
#include <dave/mpl/decay.h>

namespace dave
{
	namespace mpl
	{
		namespace transforming_iterator
		{
			struct NoStoragePolicy
			{
				struct Container { };

				template<typename R, typename I, typename F>
				R get(I&& i, F&& f) const
				{
					return f(*i);
				}

				void reset( ) { }
			};

			template<typename T>
			struct SimpleStoragePolicy
			{
				T *data = nullptr;

				~SimpleStoragePolicy( ) { reset(); }

				template<typename R, typename I, typename F>
				R get(I&& i, F&& f)
				{
					if(!data)
					{
						data = new R(f(*i));
					}

					return *reinterpret_cast<R *>(data);
				}

				template<typename R, typename I, typename F>
				R get(I&& i, F&& f) const
				{
					return data ? *reinterpret_cast<const R *>(data) : f(*i);
				}

				void reset( )
				{
					delete data; data = nullptr;
				}
			};
		}

		template
		<
			typename I,
			typename F,
			typename StoragePolicy = transforming_iterator::NoStoragePolicy
		>
		class TransformingIterator
		{
			typedef TransformingIterator<I, F, StoragePolicy> Self;

			public:
			typedef decay<typename FunctionTraits<F>::Return> value_type;

			public:
				TransformingIterator(I&& i, F&& f) : mIter(i), mCB(f) { }

				value_type operator*()
				{
					return mStorage.template get<value_type>(mIter, mCB);
				}

				value_type operator*() const
				{
					return mStorage.template get<value_type>(mIter, mCB);
				}

				Self& operator++( ) { ++mIter; mStorage.reset(); return *this; }
				Self operator++(int) { Self p{*this}; ++*this; return p; }
				Self& operator--( ) { --mIter; mStorage.reset(); return *this; }
				Self operator--(int) { Self p{*this}; --*this; return p; }

				bool operator==(const Self& i) const { return mIter == i.mIter; }
				bool operator!=(const Self& i) const { return !(*this == i); }

			private:
				I mIter;
				F mCB;
				StoragePolicy mStorage;
		};

		template<typename I, typename F>
		TransformingIterator<I, F, transforming_iterator::SimpleStoragePolicy<decay<typename FunctionTraits<F>::Return>>> transform_iterator(I&& i, F&& f)
		{
			return {forward<I>(i), forward<F>(f)};
		}
	}
}

#endif

