#ifndef DAVE_MPL_TYPELIST_H
#define DAVE_MPL_TYPELIST_H

#include <dave/mpl/types.h>
#include <dave/mpl/assert.h>
#include <dave/mpl/bool.h>

namespace dave
{
	namespace mpl
	{
		template<typename H, typename T>
		struct Cons
		{
			typedef H Head;
			typedef T Tail;
		};

		template<typename ...>
		struct MakeList;

		template<typename T, typename ... TT>
		struct MakeList<T, TT...>
		{
			typedef Cons<T, typename MakeList<TT...>::Type> Type;
		};

		template<>
		struct MakeList<>
		{
			typedef Nil Type;
		};

		template<typename ... T>
		using list = typename MakeList<T...>::Type;

		template<typename C>
		struct Car
		{
			typedef typename C::Head Type;
		};

		template<typename C>
		struct Cdr
		{
			typedef typename C::Tail Type;
		};

		template<typename C>
		using car = typename Car<C>::Type;

		template<typename C>
		using cdr = typename Cdr<C>::Type;

		template<typename C>
		struct Empty : Equals<C, Nil>
		{
		};

		template<typename C>
		struct IsList
		{
			template<typename T, typename = typename T::Head, typename = typename T::Tail>
			static True fn(T *);

			static False fn(...);

			typedef decltype(fn(declval<C *>())) Type;
			static constexpr bool Value = Type::Value;
		};

		template<typename C>
		struct Length
		{
			static constexpr int Value = 1 + Length<cdr<C>>::Value;
		};

		template<>
		struct Length<Nil>
		{
			static constexpr int Value = 0;
		};

		namespace impl
		{
			template<typename C, int N>
			struct Nth : Nth<cdr<C>, (N-1)>
			{
			};

			template<typename C>
			struct Nth<C, 0> : Car<C>
			{
			};
		}

		template<typename C, typename N>
		struct Nth : impl::Nth<C, N::Value>
		{
		};

		template<typename C, typename N>
		using nth = typename Nth<C, N>::Type;

		template<typename L, typename F>
		struct Map
		{
			typedef Cons<
				typename F::template Apply<car<L>>::Type,
				typename Map<cdr<L>, F>::Type
			> Type;
		};

		template<typename F>
		struct Map<Nil, F>
		{
			typedef Nil Type;
		};

		template<typename L, typename F>
		using map = typename Map<L, F>::Type;
	}
}

#endif

