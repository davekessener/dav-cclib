#ifndef DAVE_MPL_DECAY_H
#define DAVE_MPL_DECAY_H

namespace dave
{
	namespace mpl
	{
		template<typename T>
		struct RemoveConst
		{
			typedef T Type;
		};

		template<typename T>
		struct RemoveConst<const T> : RemoveConst<T>
		{
		};

		template<typename T>
		using remove_const = typename RemoveConst<T>::Type;

		template<typename T>
		struct RemoveVolatile
		{
			typedef T Type;
		};

		template<typename T>
		struct RemoveVolatile<volatile T> : RemoveVolatile<T>
		{
		};

		template<typename T>
		using remove_volatile = typename RemoveVolatile<T>::Type;

		template<typename T>
		struct RemoveLRef
		{
			typedef T Type;
		};

		template<typename T>
		struct RemoveLRef<T&> : RemoveLRef<T>
		{
		};

		template<typename T>
		using remove_lref = typename RemoveLRef<T>::Type;

		template<typename T>
		struct RemoveRRef
		{
			typedef T Type;
		};

		template<typename T>
		struct RemoveRRef<T&&> : RemoveRRef<T>
		{
		};

		template<typename T>
		using remove_rref = typename RemoveRRef<T>::Type;

		template<typename T>
		struct RemoveReference
		{
			typedef remove_lref<remove_rref<T>> Type;
		};

		template<typename T>
		using remove_reference = typename RemoveReference<T>::Type;

		template<typename T>
		struct Decay
		{
			typedef remove_const<remove_volatile<remove_reference<T>>> Type;
		};

		template<typename T>
		using decay = typename Decay<T>::Type;
	}
}

#endif

