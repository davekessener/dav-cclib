#ifndef DAVE_MPL_FUNCTIONTRAITS_H
#define DAVE_MPL_FUNCTIONTRAITS_H

#include <dave/mpl/types.h>
#include <dave/mpl/type_list.h>
#include <dave/mpl/decay.h>

namespace dave
{
	namespace mpl
	{
		namespace detail
		{
			template<bool C, typename R, typename K, typename ... A>
			struct fn_traits
			{
				static constexpr bool Const = C;
				typedef R Return;
				typedef K Class;
				typedef list<A...> Arguments;

				template<typename F>
				struct GetArgs
				{
					typedef typename F::template Apply<A...>::Type Type;
				};
			};
		}

		template<typename F>
		struct FunctionTraits : FunctionTraits<decltype(&decay<F>::operator())>
		{
		};

		template<typename R, typename K, typename ... A>
		struct FunctionTraits<R (K::*)(A...)> : detail::fn_traits<false, R, K, A...>
		{
		};

		template<typename R, typename K, typename ... A>
		struct FunctionTraits<R (K::*)(A...) const> : detail::fn_traits<true , R, K, A...>
		{
		};

		template<typename R, typename ... A>
		struct FunctionTraits<R (*)(A...)> : detail::fn_traits<false, R, Nil, A...>
		{
		};
	}
}

#endif

