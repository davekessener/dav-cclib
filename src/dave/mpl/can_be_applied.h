#ifndef DAVE_MPL_CANBEAPPLIED_H
#define DAVE_MPL_CANBEAPPLIED_H

#include <dave/mpl/types.h>
#include <dave/mpl/bool.h>

namespace dave
{
	namespace mpl
	{
		template<typename T, typename F>
		struct CanBeApplied
		{
			static False f(...);

			template<typename TT, bool V = F::template Apply<TT>::Value>
			static Bool<V> f(TT *);

			typedef decltype(f(std::declval<T *>())) Type;
			static constexpr bool Value = Type::Value;
		};
	}
}

#endif

