#ifndef DAVE_MPL_ASSERT_H
#define DAVE_MPL_ASSERT_H

#include <dave/mpl/types.h>

namespace dave
{
	namespace mpl
	{
		namespace impl
		{
			template<bool, typename T>
			struct Assert
			{
				typedef typename T::STATIC_ASSERTION_FAILURE Type;
			};

			template<typename T>
			struct Assert<true, T> : True
			{
			};
		}

		template<typename C, typename T>
		struct Assert : impl::Assert<C::Value, T>
		{
		};
	}
}

#endif

