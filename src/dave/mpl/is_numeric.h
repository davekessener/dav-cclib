#ifndef DAVE_MPL_ISNUMERIC_H
#define DAVE_MPL_ISNUMERIC_H

#include <dave/mpl/types.h>
#include <dave/mpl/decay.h>

namespace dave
{
	namespace mpl
	{
		template<typename T>
		struct IsNumeric
		{
			static False f(...);

			template<typename TT>
			static auto f(TT *p) -> decay<decltype(((*p) + (*p)) * ((*p) - (*p)) / ((*p) + (*p)), declval<True>())>;

			typedef decltype(f(declval<T *>())) Type;
			static constexpr bool Value = Type::Value;
		};
	}
}

#endif

