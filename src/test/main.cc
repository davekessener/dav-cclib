#include <iostream>

#include "test_ut.h"
#include "test_mpl.h"

int main(int argc, char **argv)
{
	using namespace dave;

	tests::run_mpl();
	tests::run_uts();

	return 0;
}

