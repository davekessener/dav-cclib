#include <iostream>

#include "test/test_ut.h"

#include <dave/ut/ut_main.h>

#include "test/ut_test.h"
#include "test/ut_mpl.h"

namespace dave { namespace tests {

void run_uts(void)
{
	RUN_ALL_UT;
}

}}

