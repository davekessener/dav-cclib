#include "test/ut_test.h"

namespace dave { namespace uttests {

DEFINE_UT_SUITE(SampleUT)
{
	UT_DATA
	{
		int value = 0;
	};

	UT_GLOBAL_SETUP
	{
		data->value = 42;
	};

	UT_CASE("check global setup")
	{
		UT_ASSERT(data->value == 42);
	};

	UT_CASE("test case numero uno")
	{
		data->value = 7;
	};

	UT_CASE("check if seven")
	{
		UT_ASSERT(data->value == 7);
	};

	UT_CASE("check failure")
	{
//		UT_ASSERT(data->value == 42, "Oh no, it's ", data->value, "! (This is not an error; ignore me!)");
	};
}

DEFINE_UT_SUITE(DatalessUT)
{
	UT_CASE("sample text")
	{
		UT_ASSERT(9 == 3 * 3);
	};

	UT_CASE("check invalidity of data pointer")
	{
		UT_ASSERT_EQ(data, nullptr);
	};
}

}}

