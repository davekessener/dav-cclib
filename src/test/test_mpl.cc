#include <vector>
#include <string>

#include "test_mpl.h"

#include <dave/mpl/assert.h>
#include <dave/mpl/types.h>
#include <dave/mpl/type_list.h>
#include <dave/mpl/bool.h>
#include <dave/mpl/is_iterable.h>
#include <dave/mpl/is_numeric.h>
#include <dave/mpl/function_traits.h>
#include <dave/mpl/transforming_iterator.h>

#define SASSERT(...) static_assert(::dave::mpl::Assert<__VA_ARGS__>::Value,"")

namespace dave { namespace tests {

using namespace mpl;

namespace
{
	struct Square
	{
		template<typename T>
		struct Apply : Int<(T::Value * T::Value)>
		{
		};
	};
}

int test_fn(int x) { return x * x; }

struct test_t
{
	double member_fn(int a, char b, int c) const { return 0; }
};

struct ftor
{
	char operator()(int a, double x) { return '1'; }
};

void run_mpl(void) {
	typedef list<int, double, char, void *> list_t;

	SASSERT(IsList<list_t>, list_t);
	SASSERT(Not<IsList<double>>, Nil);
	SASSERT(Equals<double, double>, True);
	SASSERT(Not<Equals<double, int>>, False);
	SASSERT(Bool<(Length<list_t>::Value == 4)>, True);
	SASSERT(Equals<Nth<list_t, Int<0>>::Type, int>, True);
	SASSERT(Equals<Nth<list_t, Int<1>>::Type, double>, True);
	SASSERT(Equals<Nth<list_t, Int<2>>::Type, char>, True);
	SASSERT(Equals<Nth<list_t, Int<3>>::Type, void *>, True);
//!	SASSERT(Equals<Nth<list_t, Int<4>>::Type, void>, True);

	typedef list<Int<1>, Int<2>, Int<3>, Int<4>, Int<5>> int_list_t;
	typedef map<int_list_t, Square> int_sq_t;

	SASSERT(Bool<(Length<int_sq_t>::Value == 5)>, True);
	SASSERT(Bool<(nth<int_sq_t, Int<2>>::Value == 9)>, True);

	SASSERT(And<True, True, True>, True);
	SASSERT(Or<False, False, True, False>, False);
	SASSERT(Not<And<True, True, False, True>>, Nil);
	SASSERT(Or<False, True, double>, double);
	SASSERT(Not<And<True, False, double>>, double);

	SASSERT(IsIterable<std::vector<int>>, std::vector<int>);
	SASSERT(Not<IsIterable<int>>, int);

	SASSERT(IsNumeric<char>, char);
	SASSERT(IsNumeric<short>, short);
	SASSERT(IsNumeric<int>, int);
	SASSERT(IsNumeric<unsigned long>, unsigned long);
	SASSERT(IsNumeric<float>, float);
	SASSERT(IsNumeric<double>, double);
	SASSERT(Not<IsNumeric<Nil>>, Nil);

	auto f = [](int x, int y) { return x * y; };

	typedef FunctionTraits<decltype(&test_fn)> fnt1_t;
	typedef FunctionTraits<decltype(f)> fnt2_t;
	typedef FunctionTraits<decltype(&test_t::member_fn)> fnt3_t;
	typedef FunctionTraits<ftor> fnt4_t;

	SASSERT(Equals<int, fnt1_t::Return>, fnt1_t);
	SASSERT(Equals<Nil, fnt1_t::Class>, fnt1_t);
	SASSERT(Bool<(!fnt1_t::Const)>, fnt1_t);
	SASSERT(Bool<(Length<fnt1_t::Arguments>::Value == 1)>, fnt1_t);
	SASSERT(Equals<Nth<fnt1_t::Arguments, Int<0>>::Type, int>, fnt1_t);

	SASSERT(Equals<int, fnt2_t::Return>, fnt2_t);
	SASSERT(Equals<decltype(f), fnt2_t::Class>, fnt2_t);
	SASSERT(Bool<fnt2_t::Const>, fnt2_t);
	SASSERT(Bool<(Length<fnt2_t::Arguments>::Value == 2)>, fnt2_t);
	SASSERT(Equals<Nth<fnt2_t::Arguments, Int<0>>::Type, int>, fnt2_t);
	SASSERT(Equals<Nth<fnt2_t::Arguments, Int<1>>::Type, int>, fnt2_t);

	SASSERT(Equals<double, fnt3_t::Return>, fnt3_t);
	SASSERT(Equals<test_t, fnt3_t::Class>, fnt3_t);
	SASSERT(Bool<fnt3_t::Const>, fnt3_t);
	SASSERT(Bool<(Length<fnt3_t::Arguments>::Value == 3)>, fnt3_t);
	SASSERT(Equals<Nth<fnt3_t::Arguments, Int<0>>::Type, int>, fnt3_t);
	SASSERT(Equals<Nth<fnt3_t::Arguments, Int<1>>::Type, char>, fnt3_t);
	SASSERT(Equals<Nth<fnt3_t::Arguments, Int<2>>::Type, int>, fnt3_t);

	SASSERT(Equals<char, fnt4_t::Return>, fnt4_t);
	SASSERT(Equals<ftor, fnt4_t::Class>, fnt4_t);
	SASSERT(Bool<(!fnt4_t::Const)>, fnt4_t);
	SASSERT(Bool<(Length<fnt4_t::Arguments>::Value == 2)>, fnt4_t);
	SASSERT(Equals<Nth<fnt4_t::Arguments, Int<0>>::Type, int>, fnt4_t);
	SASSERT(Equals<Nth<fnt4_t::Arguments, Int<1>>::Type, double>, fnt4_t);
}

}}

