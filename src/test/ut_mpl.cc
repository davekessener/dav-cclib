#include <vector>

#include "test/ut_mpl.h"

#include <dave/mpl/types.h>
#include <dave/mpl/bool.h>
#include <dave/mpl/transforming_iterator.h>
#include <dave/util/minmax.h>

namespace dave { namespace uttests {

DEFINE_UT_SUITE(MetaUT)
{
	using namespace mpl;

	UT_CASE("test transforming iterator")
	{
		std::vector<int> int_vec{1, 2, 3, 4, 5, 6, 7};
		std::vector<std::string> str_vec;

		auto convert = [](int v) {
			char s[] = { 0, 0 };
			s[0] = v + '0';
			return std::string(s);
		};

		auto i1 = transform_iterator(int_vec.begin(), convert);
		auto i2 = transform_iterator(int_vec.end(), convert);
		
		for(; i1 != i2 ; ++i1)
		{
			str_vec.push_back(*i1);
		}

		UT_ASSERT_EQ(str_vec.size(), int_vec.size());
		UT_ASSERT_EQ(str_vec.at(0), std::string("1"));
		UT_ASSERT_EQ(str_vec.at(1), std::string("2"));
		UT_ASSERT_EQ(str_vec.at(2), std::string("3"));
		UT_ASSERT_EQ(str_vec.at(3), std::string("4"));
		UT_ASSERT_EQ(str_vec.at(4), std::string("5"));
	};

	UT_CASE("test min/max")
	{
		auto v1 = util::min(3, 5.5, 2, 1.5, 42);
		auto v2 = util::max(3, 5.5, 2, 1.5, 42);

		UT_ASSERT_EQ(v1, 1.5);
		UT_ASSERT_EQ(v2, 42.0);
		UT_ASSERT((Equals<decltype(v1), double>::Value));
		UT_ASSERT((Equals<decltype(v2), double>::Value));
	};
}

}}

