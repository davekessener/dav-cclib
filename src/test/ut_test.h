#ifndef DAVE_TEST_UTTEST_H
#define DAVE_TEST_UTTEST_H

#include <dave/ut/ut_base.h>

namespace dave
{
	namespace uttests
	{
		DECLARE_UT_SUITE(SampleUT, "an example ut suite");
		DECLARE_UT_SUITE(DatalessUT, "test suite without data segment");
	}
}

#endif

